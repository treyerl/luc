﻿using LineService.Properties;
using LuciCommunication;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineService
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an instance of LuciCommunication
            Communication2Luci cl = new Communication2Luci();

            // Read commandline arguments. First one = ip of luci server. If arguments are missing, localhost is usec
            string host = "localhost";
            if (Settings.Default.luci_server != null && Settings.Default.luci_server.Length > 0)
                host = Settings.Default.luci_server;

            int instance_id = Settings.Default.instance_id;

            if (args.Count() > 0)
                int.TryParse(args[0], out instance_id);

            if (args.Count() > 1)
                host = args[1];


        //     host = "129.132.6.79";
//            host = "129.132.6.68";
            Console.Out.WriteLine("host = " + host);

            var connected = cl.connect(host, 7654);
//            LuciCommunication.Communication2Luci.LuciAnswer? a = cl.authenticate("lukas", "1234");
            if (connected)
            {
                // Register Service
                var inputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("delay", LuciType.NUMBER)
                    };

                var outputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("sum_length", LuciType.NUMBER),
                        new KeyValuePair<string, string>("instance_id", LuciType.NUMBER),
                        new KeyValuePair<string, string>("start_time", LuciType.NUMBER),
                        new KeyValuePair<string, string>("end_time", LuciType.NUMBER)
                    };

                string machinename = System.Net.Dns.GetHostName();

                LuciCommunication.Communication2Luci.LuciAnswer registered = cl.registerService("LineService", "0.1", "testing the interface", machinename, inputDef, outputDef);



                // Wait for Messages from Luci
                while (true)
                {
                   
                    // Read received Message
                    LuciCommunication.Communication2Luci.LuciAnswer resultRecv =  cl.receiveMessage();

//                    Task<Communication2Luci.LuciAnswer> la = cl.receiveMessage();
//                    LuciCommunication.Communication2Luci.LuciAnswer resultRecv = la.Result;

                    DateTime startTime = DateTime.Now;

                    Console.Out.WriteLine("----> " + resultRecv);
                    JToken r = JObject.Parse(resultRecv.Message)["action"];
                    JToken r2 = JObject.Parse(resultRecv.Message)["timestamp_inputs"];
                    JToken d = JObject.Parse(resultRecv.Message)["delay"];
                    JToken sc = JObject.Parse(resultRecv.Message)["ScID"];

                    var ddd = cl.retrieveInputs(resultRecv.Message).Where(q => q.Key == "delay").FirstOrDefault().Value;

                    long scId = 0;
                    if (sc != null)
                        scId = (long)sc;

                    LuciCommunication.Communication2Luci.LuciAnswer scenario = cl.getScenario((int)scId);
                    Console.Out.WriteLine(scenario);

                    double sum = 0;

                    if (scenario.State != Communication2Luci.LuciState.Error && scenario.Outputs != null)
                    {
                        var geom = scenario.Outputs.Where(q => q.Key == "FeatureCollection").Select(q => q.Value).First();
                        Console.Out.WriteLine("geom = " + geom);


                        if (geom != null && geom is FeatureCollection)
                        {
                            FeatureCollection fc = (FeatureCollection)geom;
                            int numPoly = fc.Count;
                            for (int f = 0; f < numPoly; f++)
                            {
                                MultiLineString mls = (MultiLineString)fc.Features[f].Geometry;
                                if (mls != null)
                                {
                                    sum += mls.Length;
                                    int numCoordinates = mls.NumPoints;
                                    for (int i = 0; i < numCoordinates; i++)
                                    {
                                        Console.Out.WriteLine(mls.Coordinates[i].X + " / " + mls.Coordinates[i].Y);
                                    }
                                }
                            }
                        }
                    }


                        long ts = 0;
                    if (r2 != null)
                        ts = (long)r2;

                    int delay = 0;
                    if (d != null)
                        delay = (int)d;

                    delay = int.Parse(ddd.ToString());

                    System.Threading.Thread.Sleep(delay);

                    int lengthSum = 1234045;

                    int objId = 0;

                    List<KeyValuePair<string, object>> results = new List<KeyValuePair<string, object>>();
                    results.Add(new KeyValuePair<string,object>("lengthSum", sum));
                    results.Add(new KeyValuePair<string, object>("instance_id", instance_id));
                    results.Add(new KeyValuePair<string, object>("start_time", startTime));
                    results.Add(new KeyValuePair<string, object>("end_time", DateTime.Now));

                    //LuciCommunication.Communication2Luci.LuciAnswer conf = 
                        cl.sendResults(objId, "0.2", "Rennsemmel", ts, results, lengthSum);
                    //Console.Out.WriteLine(conf);
                }
            }
        }
    }
}
