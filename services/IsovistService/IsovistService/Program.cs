using LuciCommunication;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.IO.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuciSerializers;

namespace IsovistService
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class Program
    {
        static void Main(string[] args)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            GeoJsonReader GeoJSONReader = new GeoJsonReader();
            Communication2Luci cl = new Communication2Luci();

            string result = "Connection to Luci was ";
            string host = "localhost";
            //host = "129.132.6.89";

            if (args.Count() > 0)
                host = args[0];

            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            var connected = cl.connect(host, 7654);

            if (connected)
            {
                Console.Out.WriteLine("-> registering service\n");

                // Register Service
                var inputDef = new List<KeyValuePair<string, string>>
                    {
                        // we expect input grid as a GeoJSON MultiPoint object
                        new KeyValuePair<string, string>("points", LuciType.STREAMINFO),
                        // Scenario ID
                        new KeyValuePair<string, string>("ScID", LuciType.NUMBER)
                    };

                var outputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("values", LuciType.JSON)
                    };

                string machinename = System.Net.Dns.GetHostName();

                LuciCommunication.Communication2Luci.LuciAnswer answer = cl.registerService("Isovist", "0.1", "Simple Demo for Isovist", host, inputDef, outputDef);
                Console.Out.WriteLine(answer.Message);

                while (true)
                {
                    LuciCommunication.Communication2Luci.LuciAnswer resultRecv = cl.receiveMessage();

                    {
                        //                        var geom = scenario.Outputs.Where(q => q.Key == "FeatureCollection").Select(q => q.Value).First();

                        JToken ts = null;
                        try { ts = JObject.Parse(resultRecv.Message)["timestamp_inputs"]; } catch (Exception){ }
                        long timestamp = 0;
                        if (ts != null)
                            timestamp = (long)ts;

                        JToken obj = null;
                        try { obj = JObject.Parse(resultRecv.Message)["SObjID"];} catch (Exception) { }
                        long objId = 0;
                        if (obj != null)
                            objId = (long)obj;


                        JToken sc = null;
                        try { sc = JObject.Parse(resultRecv.Message)["ScID"];} catch (Exception) { }
                        long scId = 0;
                        if (sc != null)
                            scId = (long)sc;

                       // {\"run\":\"Isovist\",\"ScID\":48,\"mode\":\"points\"}"
                        JToken inputs = null;
                        try { inputs = JObject.Parse(resultRecv.Message)["points"]; } catch (Exception) { }
                        List<Vector2d> points = new List<Vector2d>();
                        // prepare input values
                        if (inputs != null && inputs.HasValues)
                        {
                            var att = resultRecv.Attachments[0].Data;
                            JArray pts = inputs.Value<JArray>("coordinates");
                            int numP = pts.Count();
                            for (int j = 0; j < att.Length / 12; j++)
                            {

                                points.Add(new Vector2d((double)BitConverter.ToSingle(att, j*12), (double)BitConverter.ToSingle(att, j * 12+4)));
                            }
                        }

                        List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();

                        // fetch scenario
                        Console.Out.WriteLine("Now I will try to get scenatio");
                        var t = DateTime.Now;
                        LuciCommunication.Communication2Luci.LuciAnswer scenario = cl.getScenario((int)scId);
                        var dt = DateTime.Now.Subtract(t);
                        Console.Out.WriteLine("I got the scenario; it took " + dt);
                        //Console.Out.WriteLine(scenario);
                        Console.Out.WriteLine(scenario.State);
                        Console.Out.WriteLine(scenario.Outputs == null);

                        if (scenario.State != Communication2Luci.LuciState.Error && scenario.Outputs != null)
                        {

                            
                           // var geom = scenario.Outputs.Where(q => q.Key == "FeatureCollection").Select(q => q.Value).First();
                            var geom = ((JToken)scenario.Outputs.Where(q => q.Key == "geometry_output").Select(q => q.Value).First()).Value<object>("geometry");
                            //var geom = scenario.Outputs.Where(q => q.Key == "geometry").Select(q => q.Value).First();
                            if (geom != null && geom is FeatureCollection)
                            {
                                Console.Out.WriteLine("Trying to parse scenario");
                                t = DateTime.Now;
                                FeatureCollection fc = (FeatureCollection)geom;
                                int numPoly = fc.Count;
                                for (int f = 0; f < numPoly; f++)
                                {
                                    Geometry g = (Geometry)fc.Features[f].Geometry;
                                    if (g != null)
                                    {
                                        int numCoordinates = g.NumPoints;
                                        for (int c = 0; c < numCoordinates - 1; c++)
                                        {
                                            Coordinate coord = g.Coordinates[c];
                                            Coordinate coordNext = g.Coordinates[c + 1];

                                            lines.Add(new CPlan.Geometry.Line2D(
                                                        new Vector2d(coord.X, coord.Y),
                                                        new Vector2d(coordNext.X, coordNext.Y)
                                                      ));
                                        }
                                    }
                                }
                                dt = DateTime.Now.Subtract(t);
                                Console.Out.WriteLine("Parsed the scenario; it took " + dt);

                                Console.Out.WriteLine("Computing isovists...");
                                t = DateTime.Now;
                                //create sample iso object on client
                                CPlan.Isovist2D.IsovistField2D iso_client_orig = new CPlan.Isovist2D.IsovistField2D(points, lines, 0.05f);

                                //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
                                iso_client_orig.Calculate(iso_client_orig.Precision, true);
                                dt = DateTime.Now.Subtract(t);
                                Console.Out.WriteLine("Computed isovists; it took " + dt);

                                //wrap the results
                                IsovistResultsWrapper iso_results_wrapper = new IsovistResultsWrapper(iso_client_orig.Results);
                                //string iso_server_send = GeoJSONWriter.Write(iso_results_wrapper);

                                // send output
                                List<KeyValuePair<string, object>> results = new List<KeyValuePair<string, object>>();
                                results.Add(new KeyValuePair<string, object>("outputVals", iso_results_wrapper));

                                Console.Out.WriteLine("Done something");
                                Console.Out.WriteLine(results);
                                //LuciCommunication.Communication2Luci.LuciAnswer conf = 
                                Console.Out.WriteLine("Sending the resulsts...");
                                t = DateTime.Now;
                                cl.sendResults(objId, "0.1", host, timestamp, results, 1);
                                dt = DateTime.Now.Subtract(t);
                                Console.Out.WriteLine("Sent the results; it took " + dt);
                                //Console.Out.WriteLine(conf.Message);
                            } else
                            {
                                Console.Out.WriteLine("I could not start to parse scenario: geometry was empty!");
                            }
                        } else
                        {

                            Console.Out.WriteLine("I could not start to parse scenario: there was some error!");
                        }
                    }
                }
            }
        }
    }
}