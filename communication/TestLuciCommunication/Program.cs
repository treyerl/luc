﻿using GeoAPI.Geometries;
using LuciCommunication;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using uPLibrary.Networking.M2Mqtt;

namespace TestLuciCommunication
{
    class Program
    {
        static void Main(string[] args)
        {
            Communication2Luci cl = new Communication2Luci();
            string host = "localhost";

            // connect to luci
            cl.connect(host, 7654);
            cl.SetEnableThread(true);

            {
                /* testing solar analysis service
                string solarRes = "{'timestamp_inputs':1448555447120,'SObjID':0,'inputs':{ 'Normales':{ 'format':'doublesXYZ','streaminfo':{ 'checksum':'8B5B0977A96D30493B196D2E89BDAF74','length':8208,'order':2} },'Points':{ 'format':'doublesXYZ','streaminfo':{ 'checksum':'72B7AD79E50560EFEC7EB346893B56EC','length':8208,'order':1} },'northOrientationAngle':0,'HdriImage':{ 'format':'doublesXYZ','streaminfo':{ 'checksum':'22D70AD85B21D48147E0B0F963D02491','length':1250291,'order':3} },'calculationMode':'Klux','resolution':64},'action':'run','ScID':9}";
                var solarIn = cl.retrieveInputs(solarRes);


                float angle = 0f;
                int resolution = 64;
                string calcMode = "Klux";

                var inputSolar = new List<KeyValuePair<string, object>>
                                        {
                                            new KeyValuePair<string, object>("northOrientationAngle", angle),
                                            new KeyValuePair<string, object>("resolution", resolution),
                                            new KeyValuePair<string, object>("calculationMode", calcMode)
                                        };

                LuciCommunication.Communication2Luci.LuciStreamData solarData1 = new Communication2Luci.LuciStreamData() { Data = File.ReadAllBytes("C:\\tmp\\solar1.txt"), Format = "doublesXYZ", Name = "Points" };
                LuciCommunication.Communication2Luci.LuciStreamData solarData2 = new Communication2Luci.LuciStreamData() { Data = File.ReadAllBytes("C:\\tmp\\solar2.txt"), Format = "doublesXYZ", Name = "Normales" };
                LuciCommunication.Communication2Luci.LuciStreamData solarData3 = new Communication2Luci.LuciStreamData() { Data = File.ReadAllBytes("C:\\tmp\\solar3.txt"), Format = "doublesXYZ", Name = "HdriImage" };


                // run Service asynchronously
                LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = cl.runService("SolarAnalysis", inputSolar, 9, solarData1, solarData2, solarData3);
                */

                try
                {
                    // test to run built-in service FileEcho 
                    // take care to provide the two files or change the names to existing files
                    var kvp = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("one", 1),
                        new KeyValuePair<string, object>("oans", "eins"),
                        new KeyValuePair<string, object>("two", 2),
                        new KeyValuePair<string, object>("zwei", new int[]{1,2,3,4}),
                        new KeyValuePair<string, object>("three", 3)
                    };

                    // make sure to have the 2 files or replace string by names of existing files
                    byte[] fileData = File.ReadAllBytes("..\\..\\TestFileEcho.png");
                    Communication2Luci.LuciStreamData streamData = new Communication2Luci.LuciStreamData() { Data = fileData, Format = "jpg", Name = "Giröfflein" };

                    LuciCommunication.Communication2Luci.LuciAnswer answerFileEcho = cl.runService("test.FileEcho", null, -1, streamData);
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.Message);
                    Console.Out.WriteLine(ex.StackTrace);
                }


                // test to get lists
                string[] listScenarios = cl.getScenarioList();
                string[] listServices = cl.getServicesList();
                Console.Out.WriteLine(listScenarios);
                Console.Out.WriteLine(listServices);


                // create scenario
                Point[] ptsF = new Point[4];
                ptsF[0] = new Point(102, 0);
                ptsF[1] = new Point(103, 13);
                ptsF[2] = new Point(104, 0);
                ptsF[3] = new Point(105, 1);
                NetTopologySuite.Geometries.MultiPoint mpF = new MultiPoint(ptsF);
                FeatureCollection featurecollection;
                System.Collections.ObjectModel.Collection<IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

                AttributesTable pointAttr = new AttributesTable();
                pointAttr.AddAttribute("layer", "buildingHeight");
                features.Add(new Feature(mpF, pointAttr));
                featurecollection = new FeatureCollection(features);


                int scenarioID = -1;
                LuciCommunication.Communication2Luci.LuciAnswer luciAnswer = cl.createScenario("cplan", featurecollection, null);
                int.TryParse(cl.GetOutputValue(luciAnswer, "ScID").ToString(), out scenarioID);


                // test to get scenaorio
                LuciCommunication.Communication2Luci.LuciAnswer a1 = cl.getScenario(scenarioID);

                // get a layer of a scenario
                LuciCommunication.Communication2Luci.LuciAnswer a2 = cl.getScenario(scenarioID, "buildingHeight");

                // test to register a service
                var inputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("delay", LuciType.NUMBER),
                        new KeyValuePair<string, string>("num_lines", LuciType.NUMBER)
                    };

                var outputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("sum_length", LuciType.NUMBER),
                        new KeyValuePair<string, string>("sum_lines", LuciType.NUMBER)
                    };

                string machinename = System.Net.Dns.GetHostName();
                LuciCommunication.Communication2Luci.LuciAnswer answerRegister = cl.registerService("TestLineService", "0.1", "testing the interface", machinename, inputDef, outputDef);


                // call a async service
                /*
                                var kvpLL = new List<KeyValuePair<string, object>>
                                    {
                                        new KeyValuePair<string, object>("delay", 5000),
                                    };
                                MainAsync(cl, kvpLL).Wait();
                */

                // create scenario for LineService
                GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                int numLines = 20;
                Random rnd = new Random();
                ILineString[] lines = new ILineString[numLines];

                for (int i = 0; i < numLines; i++)
                {
                    Coordinate[] coords = new Coordinate[2];
                    coords[0] = new Coordinate(rnd.Next(100), rnd.Next(50));
                    coords[1] = new Coordinate(rnd.Next(100), rnd.Next(50));

                    lines[i] = new LineString(coords);
                }

                NetTopologySuite.Geometries.MultiLineString multiLines = new NetTopologySuite.Geometries.MultiLineString(lines);

                int scid_LS = -1;

                LuciCommunication.Communication2Luci.LuciAnswer laScLine = cl.createScenario("LineService Test", multiLines, null);
                if (laScLine.Outputs != null)
                {
                    var scID = laScLine.Outputs.Where(q => q.Key == "ScID");
                    if (scID != null && scID.Any())
                        scid_LS = int.Parse(scID.First().Value.ToString());
                }


                if (scid_LS >= 0)
                {
                    LuciCommunication.Communication2Luci.LuciAnswer scenario = cl.getScenario((int)scid_LS);


                    // test to run LineService (start it first)
                    var inputLS = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", 13)
                    };

                    LuciCommunication.Communication2Luci.LuciAnswer? aLS = cl.runService("LineService", inputLS, scid_LS);
                    Console.Out.WriteLine(aLS);

                    // for loop to test stability
                    for (int i = 0; i < 100; i++)
                        cl.runService("LineService", inputLS, scid_LS);
                }




                // create input parameters for isovist service
                Point[] pts = new Point[4];
                pts[0] = new Point(102, 0);
                pts[1] = new Point(103, 13);
                pts[2] = new Point(104, 0);
                pts[3] = new Point(105, 1);
                NetTopologySuite.Geometries.MultiPoint mp = new MultiPoint(pts);
                string geoJSONPts = new GeoJsonWriter().Write(mp);
                var inputIS = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("inputVals", mp)
                    };


                // create scenario for isovist service
                int index = 0;

                List<System.Windows.Rect> buildings = new List<System.Windows.Rect>();
                buildings.Add(new System.Windows.Rect(10.1, 10.1, 100.1, 100.1));
                buildings.Add(new System.Windows.Rect(50.2, 50.2, 200.2, 200.2));
                buildings.Add(new System.Windows.Rect(3000.0, 2000, 1000.3, 222.3));

                FeatureCollection fc = new FeatureCollection();
                AttributesTable at = new AttributesTable();
                at.AddAttribute("layer", "buildings");
                at.AddAttribute("geomID", 22);

                NetTopologySuite.Geometries.Polygon poly = null;
                NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count];
                double OffsetX = 0;
                double OffsetY = 0;
                foreach (System.Windows.Rect r in buildings)
                {
                    LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(new Coordinate[]
                    {
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left),
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Right),
                    new Coordinate(OffsetX + r.Top, OffsetY + r.Right),
                    new Coordinate(OffsetX + r.Top, OffsetY + r.Left),
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left)
                    }
                    );
                    NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);

                    AttributesTable at1 = new AttributesTable();
                    at1.AddAttribute("layer", "footprints");
                    at1.AddAttribute("geomID", index + 1);
                    at1.AddAttribute("footprintID", (index + 1) * 13);
                    fc.Add(new Feature(p, at1));
                    polyArray[index] = p;
                    poly = p;
                    index++;
                }
                MultiPolygon mPoly = new MultiPolygon(polyArray);
                string geoJSONPoly = GeoJSONWriter.Write(mPoly);
                geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);

                // for loop to test db connection bug
                for (int i = 0; i < 100; i++)
                    cl.createScenario("test", mPoly, null);

                LuciCommunication.Communication2Luci.LuciAnswer aLS4 = cl.createScenario("test", mPoly, null);
                Console.Out.WriteLine(aLS4);

                if (aLS4.Outputs != null)
                {
                    int sid = -1;
                    var scID = aLS4.Outputs.Where(q => q.Key == "ScID");
                    if (scID != null && scID.Any())
                        sid = int.Parse(scID.First().Value.ToString());
                }

                int ii = int.Parse(cl.GetOutputValue(aLS4, "ScID").ToString());
                Console.Out.WriteLine("scid = " + ii);


                LuciCommunication.Communication2Luci.LuciAnswer? aISO = cl.runService("Isovist", inputIS, ii);
                Console.Out.WriteLine(aISO);

                // create service with feature collection and attributes
                LuciCommunication.Communication2Luci.LuciAnswer aLS5 = cl.createScenario("test_layer", fc, null);
                Console.Out.WriteLine(aLS5);
            }

        }

        static async Task MainAsync(Communication2Luci cl, List<KeyValuePair<string, object>> inputLS)
        {
            // test to run LineService asynchronously via MQTT
            /*            LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await cl.runAsyncService("LineService", inputLS, 513);
                        await Task.Delay(1);
                        Console.Out.WriteLine(asyncAnswer);*/
        }
    }
}
